import React from "react";
import ReactDom from "react-dom/client";
import "bootstrap/dist/css/bootstrap.min.css";
import "./query.css";
import {
  HeroSection,
  Navbar,
  About,
  Skillheading,
  Contact,
  Footer,
} from "./App.js";

const Section1 = [
  {
    id: 1,
    Name: "Python",
    Image: "/Asset/Skills/python-48.png",
    Discription:
      "Proficient in Python for Machine Learning and Data Analysis. I leverage its robust libraries to extract insights, build models, and contribute to the evolving landscape of data-driven solutions",
    Field: "Programming Languages",
  },
  {
    id: 2,
    Name: "C/C++",
    Image: "/Asset/Skills/c_pro.png",
    Discription:
      "Proficient in C and C++ for IoT applications. I specialize in developing intelligent systems that seamlessly integrate with the Internet of Things (IoT). My expertise lies in crafting robust",
    Filed: "Programming Languages",
  },
  {
    id: 3,
    Name: "SQL",
    Image: "/Asset/Skills/sql-50.png",
    Discription:
      "Proficient in SQL, I excel in database management, design, and optimization. With a strong grasp of SQL queries, indexing, and normalization, I ensure seamless data management",
    Filed: "Programming Languages",
  },
];

const Section2 = [
  {
    id: 4,
    Name: "Machine Learning",
    Image: "/Asset/Skills/machine-learning.png",
    Discription:
      " Passionate about the endless possibilities of machine learning, I specialize in developing and deploying models that extract valuable insights from complex data, contributing to data-driven innovations",
    Filed: "Technology",
  },
  {
    id: 5,
    Name: "Data Science",
    Image: "/Asset/Skills/data-science.png",
    Discription:
      "Dedicated to unraveling patterns and uncovering insights, I apply statistical methods and machine learning techniques to derive meaningful conclusions from diverse datasets, driving informed decision-making in the realm of data science",
    Filed: "Technology",
  },
  {
    id: 6,
    Name: "Robotics",
    Image: "/Asset/Skills/robotic-48.png",
    Discription:
      "Combining innovation and technology, I delve into the dynamic world of robotics, crafting intelligent systems that seamlessly interact with the physical world. With a passion for pushing the boundaries of what's possible",
    Filed: "Technology",
  },
];

function Myapp() {
  return (
    <div>
      <Navbar />
      <HeroSection />
      <About />
      <Skillheading />
      <div className="container">
        <div className="sperate_skill">
          {Section1.map((data) => (
            <Section1Skill skilldata={data} key={data.id} />
          ))}
        </div>
        <div className="sperate_skill" style={{ marginTop: "50px" }}>
          {Section2.map((data) => (
            <Section2Skill skilldata={data} key={data.id} />
          ))}
        </div>
      </div>
      <div className="container-opt opticaity-small Show-content">
        <SmallScreentigger skilldatasmall={Section1[0]} />
        <SmallScreentigger skilldatasmall={Section2[0]} />
        <SmallScreentigger skilldatasmall={Section2[2]} />
      </div>
      <Contact />
      <Footer />
    </div>
  );
}

function Section1Skill({ skilldata }) {
  return (
    <div className="container opticaity-large">
      <div className="row">
        <div
          className="card"
          style={{
            width: "20rem",
            background: "#2B2D41",
          }}
        >
          <img
            src={skilldata.Image}
            className="card-img-top mx-2"
            alt="skill name"
            style={{ width: "60px", paddingTop: "40px" }}
          />
          <div className="card-body">
            <h5
              className="card-title text-light"
              style={{ fontWeight: "bolder" }}
            >
              {skilldata.Name}
            </h5>
            <p className="card-text text-light">{skilldata.Discription}</p>
            <a href="#" className="btn btn-primary">
              Click More
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}

function Section2Skill({ skilldata }) {
  return (
    <div className="container">
      <div className="row opticaity-large">
        <div
          className="card"
          style={{
            width: "20rem",
            background: "#2B2D41",
          }}
        >
          <img
            src={skilldata.Image}
            className="card-img-top mx-2"
            alt="skill name"
            style={{ width: "60px", paddingTop: "40px" }}
          />
          <div className="card-body">
            <h5
              className="card-title text-light"
              style={{ fontWeight: "bolder" }}
            >
              {skilldata.Name}
            </h5>
            <p className="card-text text-light">{skilldata.Discription}</p>
            <a href="#" className="btn btn-primary">
              Click More
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}

function SmallScreentigger({ skilldatasmall }) {
  return (
    <div className="container Show-content">
      <div className="row">
        <div
          className="card"
          style={{
            width: "20rem",
            background: "#2B2D41",
          }}
        >
          <img
            src={skilldatasmall.Image}
            className="card-img-top mx-2"
            alt="skill name"
            style={{ width: "60px", paddingTop: "40px" }}
          />
          <div className="card-body">
            <h5
              className="card-title text-light"
              style={{ fontWeight: "bolder" }}
            >
              {skilldatasmall.Name}
            </h5>
            <p className="card-text text-light">{skilldatasmall.Discription}</p>
            <a href="#" className="btn btn-primary">
              Click More
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}

const root = ReactDom.createRoot(document.querySelector("#root"));
root.render(<Myapp />);
