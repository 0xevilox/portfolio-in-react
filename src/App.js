import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.min.js";
import "./index.css";
import "./query.css";
export function Navbar() {
  return (
    <div className="bg-dark">
      <div className="container">
        <div className="row">
          <nav className="navbar navbar-expand-lg py-3">
            <div className="container-fluid">
              <a className="navbar-brand logo text-light" href="#">
                <h3>Krishna</h3>
              </a>
              <button
                className="navbar-toggler"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <span className="navbar-toggler-icon"></span>
              </button>
              <div
                className="collapse navbar-collapse"
                id="navbarSupportedContent"
              >
                <ul className="navbar-nav ms-auto mb-2 mb-lg-0 nav-links_fonts Down nav-link-custom d-flex">
                  <li className="nav-item">
                    <a
                      className="nav-link active text-light"
                      aria-current="page"
                      href="#home"
                    >
                      Home
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link text-light" href="#about">
                      About
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link text-light" href="#skills">
                      Skills
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link text-light" href="#contact">
                      Contact
                    </a>
                  </li>
                  <ul className="dropdown-menu">
                    <li>
                      <a className="dropdown-item" href="#">
                        Action
                      </a>
                    </li>
                    <li>
                      <a className="dropdown-item" href="#">
                        Another action
                      </a>
                    </li>
                    <li>
                      <hr className="dropdown-divider" />
                    </li>
                    <li>
                      <a className="dropdown-item" href="#">
                        Something else here
                      </a>
                    </li>
                  </ul>
                </ul>
              </div>
            </div>
          </nav>
        </div>
      </div>
    </div>
  );
}

export function HeroSection() {
  const [CurrentName, ChangeName] = useState("Machine Learning");
  useEffect(() => {
    const delay = 3000;
    const timer = setInterval(() => {
      ChangeName((prevName) =>
        prevName === "Machine Learning" ? "C/C++" : "Machine Learning"
      );
    }, delay);
    return () => clearTimeout(timer);
  }, []);
  return (
    <section id="Home">
      <div className="container">
        <div className="row">
          <div className="col-md-6 py-5 my-5">
            <div className="hero_section py-5 w3-animate-left">
              <h1 className="hero_section--heading text-light">
                Hello, I'm a <br />
                {CurrentName} Developer
              </h1>
              <p className="hero_section--intro text-secondary">
                Passionate about leveraging cutting-edge technology <br /> to
                solve complex problems, I specialize in developing <br />
                intelligent systems and algorithms.
              </p>
              <a href="#contact">
                <button type="button" className="btn btn-primary btn-lg ">
                  Contact Me
                </button>
              </a>
            </div>
          </div>
          <div className="col-6">
            <img
              className="my-4 w3-animate-right"
              src="/Asset/background.png"
              alt="hacker image"
              width={"600px"}
              height={"500px"}
            />
          </div>
        </div>
      </div>
    </section>
  );
}

export function Heading(props) {
  return (
    <div>
      <h1 className="p-2" style={{ color: "#DC4452", fontWeight: "bold" }}>
        {props.name}
      </h1>
      <div className="line decrease-width" style={{ width: props.width }}></div>
    </div>
  );
}

export function About() {
  return (
    <section id="about">
      <div className="container">
        <div className="row">
          <div className="col-6 py-5 my-5">
            <Heading name="About Me" width="220px" />
            <img
              className="py-4"
              src="/Asset/krishna.jpg"
              alt="Sri krishna Image"
              width={"300px"}
              style={{ cursor: "pointer" }}
            />
          </div>
          <div className="col-md-6  py-5 my-5">
            <div className="about_us py-5 my-5">
              <p className="about_us-Name h1 text-light">Shri Krishnaa P</p>
              <p className="about_us-me text-light">
                Hi, I'm Shri Krishnaa P, currently pursuing the pre-final year
                of my B.Tech in Artificial Intelligence and Data Science at
                Panimlar Institute of Technology. I have a strong passion for
                technology and am dedicated to learning and applying my skills
                in various domains
              </p>
              <div className="about_us-image-container py-4 ">
                <img
                  src="/Asset/fb.svg"
                  style={{ paddingRight: "10px", cursor: "pointer" }}
                  alt="Facebook"
                />
                <img
                  src="/Asset/insta.svg"
                  style={{ padding: "10px", cursor: "pointer" }}
                  alt="Facebook"
                />
                <img
                  src="/Asset/link.svg"
                  style={{ padding: "10px", cursor: "pointer" }}
                  alt="Facebook"
                />
              </div>
              <a href="#">
                <button className="btn btn-outline-primary">
                  Download Resume
                </button>
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
export function Skillheading() {
  return (
    <section id="skills">
      <div className="container">
        <Heading name="Skill-Set" width="200px" />;
      </div>
    </section>
  );
}

export function Contact() {
  return (
    <section id="contact" className="py-5 my-5">
      <div className="container">
        <Heading name="Contact Me" width="250px" />;
        <div className="row">
          <div className="col-6">
            <div className="forms">
              <form>
                <div className="mb-3 increase-width">
                  <label
                    htmlFor="exampleInputEmail1"
                    className="form-label text-light"
                  >
                    Name
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    id="exampleInputEmail1"
                    aria-describedby="text"
                    placeholder="Please Enter Your Full Name"
                  />
                  <br />
                  <label
                    htmlFor="exampleInputEmail1"
                    className="form-label text-light"
                  >
                    Email address
                  </label>
                  <input
                    type="email"
                    className="form-control"
                    id="exampleInputEmail1"
                    aria-describedby="emailHelp"
                    placeholder="Please Enter Your Working Mail-ID "
                  />
                  <div id="emailHelp" className="form-text">
                    We'll never share your email with anyone else.
                  </div>
                  <label
                    htmlFor="exampleFormControlTextarea1"
                    className="form-label text-light"
                  >
                    Message
                  </label>
                  <textarea
                    className="form-control"
                    id="exampleFormControlTextarea1"
                    rows="3"
                    placeholder="Please Enter Your Message"
                  ></textarea>
                </div>
                <a href="#">
                  <button type="button" className="btn btn-primary btn-lg">
                    Submit
                  </button>
                </a>
              </form>
            </div>
          </div>
          <div className="col-6"></div>
        </div>
      </div>
    </section>
  );
}

export function Footer() {
  return (
    <div className="container text-light text-center">
      <footer style={{ padding: "15px" }}>
        © Copyright to Sri Krishnaa and Made By Vignesh
      </footer>
    </div>
  );
}
